﻿namespace BitcrackRandomiser.Enums
{
    /// <summary>
    /// Type of external app.
    /// </summary>
    public enum AppType
    {
        /// <summary>
        /// Bitcrack
        /// </summary>
        bitcrack
    }
}
